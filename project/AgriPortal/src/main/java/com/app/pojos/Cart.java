package com.app.pojos;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="cart")
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class Cart extends BaseEntity {
//productId, name ,price,description,inStock	
	private double quantity;
	
	@OneToOne
	private Users user;
	
	@ManyToMany
	@JoinTable(name = "product_cart", joinColumns = @JoinColumn(name = "cid"), inverseJoinColumns = @JoinColumn(name ="pid"))
	private List<Product> products;
		
	}

