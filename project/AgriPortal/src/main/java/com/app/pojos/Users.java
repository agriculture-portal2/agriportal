package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Users extends BaseEntity {

	
	@Column(name = "first_name", length = 15, nullable = false)
	private String firstname;
	

	@Column(name = "last_name", length = 15)
	private String lastname;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "user_role", length = 30)
	@NotNull(message = "user role must be supplied!")
	private Role role;
	
	@Column(name = "mobile_no")
	@Pattern(regexp = "(^(\\+91[\\-\\s]?)?[0]?(91)?[789]\\d{9}$)")
	@NotNull(message = "mobile number must be supplied!")
	private String mobileno;
	
	@Column(length = 25, unique = true, nullable = false)
	private String email;
	
	
	@Column(length = 20, nullable = false)
	private String password;
	
	@Embedded
	private Address address;
	
	@OneToOne
	@JoinColumn(name = "pincode")
	private PinDetails pincode;
}
