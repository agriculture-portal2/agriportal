package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Product extends BaseEntity {

	@Column(name="product_name",length = 20,unique = true)
	private String productName;
	
	private double price;
	
	private String expiryDate;
	
	@Column(length = 250)
	private String description;
	
	@Column(nullable = false)
	private double quantity;
	
	@ManyToOne
	@JoinColumn(name = "uid")
	private Users user;
	
	@ManyToOne
	@JoinColumn(name = "sbt_id")
	private SubCategories subCategories;
	
	@ManyToMany
	@JoinTable(name = "product_order",joinColumns=@JoinColumn(name="pid"),inverseJoinColumns=@JoinColumn(name="oid"))
	private List<OrderHistory> orders= new ArrayList<>();
	
}
