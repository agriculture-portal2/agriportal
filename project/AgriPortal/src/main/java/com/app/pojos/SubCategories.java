package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SubCategories extends BaseEntity {

	@Column(name = "sub_categories" ,length=50)
	private String subcategories;
	
	@ManyToOne
	@JoinColumn(name = "ct_id")
	private Categories categories;
	
}

