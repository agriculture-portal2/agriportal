package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "pin_details")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PinDetails {

	@Id
	@Column(length = 6)
	private String pincode;
	@Column(length = 30,unique = true, nullable = false )
	private String city;
}
