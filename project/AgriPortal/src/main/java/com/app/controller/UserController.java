package com.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Users;

@RestController
@RequestMapping("/user")
public class UserController {
	
	
	public UserController() {
		System.out.println("Inside UserController ctor .......");
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@RequestBody Users user){
		
		return null;
	}
	
	@PostMapping("/viewprofile")
	public Users viewProfile(Long id)  {
		
		return null;
	}
	
	@PostMapping("/editprofile")
	public String editProfile(Long id, Users user)  {
		
		return "Profie edited successfully";
	}
}
