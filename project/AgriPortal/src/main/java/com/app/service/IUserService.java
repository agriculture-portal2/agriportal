package com.app.service;

import java.util.Optional;

import com.app.pojos.Users;

public interface IUserService {
	
	Optional<Users> viewprofile(Long id);
	
	String editProfile(Long id, Users customer);
}
